﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AngularJSAuthentication.API.Models
{
    public class CustomExtensions
    {
    }

    public static class CelloClaimTypes
    {
        public const string ClientId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/clientid";
        public const string LoggedInTenantId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/loggedintenantid";
        public const string LoggedInUserRoles = "http://schemas.microsoft.com/ws/2008/06/identity/claims/loggedinuserroles";
        public const string LoggedInUserTenantId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/loggedinusertenantid";
        public const string Privileges = "http://schemas.microsoft.com/ws/2008/06/identity/claims/privileges";
        public const string Profile = "http://schemas.microsoft.com/ws/2008/06/identity/claims/profile";
        public const string Scopes = "http://schemas.microsoft.com/ws/2008/06/identity/claims/scopes";
        public const string SessionRoles = "http://schemas.microsoft.com/ws/2008/06/identity/claims/sessionroles";
        public const string SessionTenantId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/sessiontenantid";
        public const string TenantId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/tenantid";
        public const string UserId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/userid";

    }

    public static class CelloConstants
    {
        public const string MultiTenantClientID = "5DF78E31-1DCA-4DCD-832F-C28B599FFF5B";
        public const string ExternalTenantId = "f5fc51fa-f668-41fc-ba9d-8cc36a073713";
    }
}