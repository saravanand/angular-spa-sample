﻿using AngularJSAuthentication.API.Models;
using AngularJSAuthentication.API.Providers;
using CelloSaaS.Owin.Security.GenericOAuth2Authentication;
using CelloSaaS.Owin.Security.GenericOAuth2Authentication.Provider;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AppFunc = System.Func<System.Collections.Generic.IDictionary<string, object>, System.Threading.Tasks.Task>;

[assembly: OwinStartup(typeof(AngularJSAuthentication.API.Startup))]

namespace AngularJSAuthentication.API
{
    public class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        public static GoogleOAuth2AuthenticationOptions googleAuthOptions { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            ConfigureOAuth(app);

            WebApiConfig.Register(config);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            app.UseWebApi(config);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AuthContext, AngularJSAuthentication.API.Migrations.Configuration>());

        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            //use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {

                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(30),
                Provider = new SimpleAuthorizationServerProvider(),
                RefreshTokenProvider = new SimpleRefreshTokenProvider()
            };

            app.Use(new Func<AppFunc, AppFunc>(next => (async context =>
            {
                Debug.Write("Executed");
                await next.Invoke(context);
            })));

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

            //Configure Google External Login
            googleAuthOptions = new GoogleOAuth2AuthenticationOptions()
            {
                ClientId = "182907251249-quah5ff6qdn0lkk224jo2mepm1uihkge.apps.googleusercontent.com",
                ClientSecret = "qZMRgGaI5yZe1PdxkMY_xE-j",
                Provider = new GoogleAuthProvider()
            };
            app.UseGoogleAuthentication(googleAuthOptions);

            //cello_help: Middlewares: Register the Cello Middleware to talk to the cellosaas authorization server
            var authOptions = new GenericOAuth2AuthenticationOptions("CelloAuth")
            {
                SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie,
                AccessType = "CelloAuth",
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive,
                AuthorizationEndpointUri = "http://localhost:37202/authorize",
                TokenEndpointUri = "http://localhost:37202/api/token",
                UserInfoEndpointUri = "http://localhost:37202/api/userinfo",
                Scope = new List<string> { "all" },
                Provider = new GenericOAuth2AuthenticationProvider
                {
                    OnAuthenticated = ctx =>
                    {
                        ctx.Identity.AddClaim(new Claim("ExternalAccessToken", ctx.AccessToken));
                        return System.Threading.Tasks.Task.FromResult<object>(null);

                    },
                    InitRequestFromAuthenticationProperties = EndpointUriTypes => true,
                    OnMapContextToClaims = async context =>
                    {
                        List<Claim> additionalClaims = new List<Claim>();
                        if (!string.IsNullOrEmpty(context.Id))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.UserId, context.Id));
                        }
                        if (!string.IsNullOrEmpty(context.ClientId))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.ClientId, context.ClientId));
                        }
                        if (!string.IsNullOrEmpty(context.TenantId))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.TenantId, context.TenantId));
                            additionalClaims.Add(new Claim(CelloClaimTypes.LoggedInTenantId, context.TenantId));
                        }
                        if (!string.IsNullOrEmpty(context.Scope))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.Scopes, context.Scope));
                        }
                        if (!string.IsNullOrEmpty(context.Profile))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.Profile, context.Profile));
                        }

                        if (!string.IsNullOrEmpty(context.Role))
                        {
                            additionalClaims.Add(new Claim(ClaimTypes.Role, string.Join(",", context.Role)));
                            additionalClaims.Add(new Claim(CelloClaimTypes.LoggedInUserRoles, string.Join(",", context.Role)));
                        }
                        if (context.ExpiresIn != null && context.ExpiresIn.HasValue && context.ExpiresIn.Value.TotalSeconds > 0)
                        {
                            additionalClaims.Add(new Claim(ClaimTypes.Expiration, context.ExpiresIn.Value.TotalSeconds.ToString()));
                        }
                        return additionalClaims;
                    }
                }
            };
            app.UseGenericOAuth2Authentication(authOptions);
        }

        public Task Invoke(IDictionary<string, object> environment)
        {
            return null;
        }
    }

}