# README #

This is a sample Angular application that uses Authorization Server to get the user authenticated and then uses the token received to proceed with the application logic.

### Version Compatibility ###
* Version 4.5.0.0


### Who do I talk to? ###
* Saravanan [saravanan.d@techcello.com]
* Other community or team contact [support@techcello.com]