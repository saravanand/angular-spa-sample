﻿'use strict';
app.factory('ordersService', ['$http', 'ngAuthSettings', function ($http, ngAuthSettings) {

    //var serviceBase = ngAuthSettings.apiServiceBaseUri;

    var ordersServiceFactory = {};

    var _getOrders = function () {

        return $http.get('http://localhost:26264/api/orders').then(function (results) {
            //return $http.get(serviceBase).then(function (results) {
            return results;
        });
    };

    var _getUsers = function () {
        serviceBase = "http://localhost:60922/api/users/get_all_users"
        return $http.get(serviceBase).then(function (results) {
            return results;
        });
    }

    ordersServiceFactory.getOrders = _getOrders;
    ordersServiceFactory.getUsers = _getUsers;

    //serviceBase = "http://localhost:26265/";

    return ordersServiceFactory;

}]);