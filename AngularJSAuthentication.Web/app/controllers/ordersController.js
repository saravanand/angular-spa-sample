﻿'use strict';
app.controller('ordersController', ['$scope', 'ordersService', function ($scope, ordersService) {

    $scope.orders = [];

    ordersService.getOrders().then(function (results) {

        $scope.orders = results.data;

    }, function (error) {
        //alert(error.data.message);
    });

    $scope.users = [];

    ordersService.getUsers().then(function (results) {

        $scope.users = results.data;

    }, function (error) {
        //alert(error.data.message);
    });



}]);